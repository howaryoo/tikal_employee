import datetime

from pydantic import BaseModel


class EmployeeBaseModel(BaseModel):
    class Config:
        orm_mode = True


class Employee(EmployeeBaseModel):
    name: str
    group_name: str
    start_date: datetime.date


class EmployeeOut(Employee):
    id: int


class Task(EmployeeBaseModel):
    customer_name: str
    employee_id: int
    start_date: datetime.date
