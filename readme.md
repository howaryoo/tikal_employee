# Fastapi teaser demo

Batteries included:
 
 * swagger UI
 * tests
 * migrations

## Demo

    > docker-compose up -d

and point your browser to: http://localhost:8000/docs

## tests

run API tests with:

    > make test

tests use a request client fixture provided by the FW that allows:
 to execute requests to the API without binding to a IP/port number.

## Deploy with app servers and worker classes

little lies, big lies and benchmarks:
https://gitlab.com/howaryoo/quart-benchmark


## History


 * Async: Twisted - Tornado --->  asyncio based new frameworks
 * Sync: Flask, Flacon ...


## Refs:

https://techspot.zzzeek.org/2015/02/15/asynchronous-python-and-databases/
https://ahmed-nafies.medium.com/fastapi-with-sqlalchemy-postgresql-and-alembic-and-of-course-docker-f2b7411ee396
https://github.com/leosussan/fastapi-gino-arq-uvicorn
