.PHONY: all test clean

SHELL := $(shell which bash)
PROJECT := tikal_employee

clean:
	find . -name '*.py[co]' -delete
	find . -name '*.skp' -delete
	find . -name '.coverage*' -delete
	rm -rf build dist $(PROJECT).egg-info
	find . -path '*/__pycache__*' -delete
	find . -path '*/.pytest_cache*' -delete
	find . -path '*/cov.xml' -delete
	rm -rf report
	rm -rf htmlcov

validate: clean
	flake8 api


build:
	docker-compose -f docker-compose.yml -f docker-compose-test.yml build

test:
	docker-compose -f docker-compose.yml -f docker-compose-test.yml run --rm test

%:
	@:
