def test_crud(client):
    create = client.post(
        '/employee',
        json={
            "name": "asdf",
            "group_name": "g1",
            "start_date": "2000-01-01",
        },
    )
    assert create.status_code == 200
    assert create.json()["id"], "id not returned"

    get = client.get(
        "/employee?name=asdf"
    )

    assert get.status_code == 200
    assert isinstance(get.json(), list), "expected list"
    assert len(get.json()), "missing records"

