from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy import Column, Integer, String, Date, ForeignKey

Base = declarative_base()


class Employee(Base):
    __tablename__ = "employee"
    id = Column(Integer, primary_key=True, index=True)
    name = Column(String)
    group_name = Column(String)
    start_date = Column(Date)


class Task(Base):
    __tablename__ = "task"
    id = Column(Integer, primary_key=True, index=True)
    customer_name = Column(String)
    employee_id = Column(Integer, ForeignKey('employee.id'))
    start_date = Column(Date)
