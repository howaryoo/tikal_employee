import uvicorn
from fastapi import FastAPI

import os
from fastapi_sqlalchemy import DBSessionMiddleware
from fastapi_sqlalchemy import db
from pydantic.typing import List

from domain import models
import schema
from dotenv import load_dotenv

BASE_DIR = os.path.dirname(os.path.abspath(__file__))
load_dotenv(os.path.join(BASE_DIR, ".env"))


app = FastAPI()
app.add_middleware(DBSessionMiddleware, db_url=os.environ["DATABASE_URL"])


@app.post("/employee", response_model=schema.EmployeeOut)
def create_employee(employee: schema.Employee):
    db_employee = models.Employee(
        name=employee.name,
        group_name=employee.group_name,
        start_date=employee.start_date,
    )
    db.session.add(db_employee)
    db.session.commit()
    return db_employee


@app.get("/employee", response_model=List[schema.EmployeeOut])
def get_employee(name: str, group: str = None):
    employees = db.session.query(models.Employee).filter(
        models.Employee.name.ilike(f"%{name}%")
    )
    if group:
        employees = employees.filter(
            models.Employee.group_name == group
        )
    return employees.limit(10).all()


@app.post("/task", response_model=schema.Task)
def create_task(task: schema.Task):
    db_task = models.Task(
        customer_name=task.customer_name,
        employee_id=task.employee_id,
        start_date=task.start_date,
    )
    db.session.add(db_task)
    db.session.commit()
    return db_task


if __name__ == "__main__":
    uvicorn.run(app, host="0.0.0.0", port=8000)
